<?php

use App\Http\Controllers\V1\Auth\UserLoginController;
use App\Http\Controllers\V1\Auth\UserRegisterController;
use App\Http\Controllers\V1\ProductController;
use App\Http\Controllers\V1\BookmarkController;
use App\Http\Middleware\CheckAdminMiddleware;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->name('auth')->group(function () {
    Route::post('/register', [UserRegisterController::class, 'store'])->name('.register');
    Route::post('/login', [UserLoginController::class, 'store'])->name('.login');
});

Route::prefix('admin')->name('admin')->middleware(['auth:api', CheckAdminMiddleware::class])->group(function () {
    Route::apiResource('product', ProductController::class)->except(['index', 'show']);
});

Route::prefix('user')->middleware(['auth:api'])->group(function () {
    Route::get('product/{product_id}', [ProductController::class, 'show']);
    Route::get('bookmarks', [BookmarkController::class, 'index']);
    Route::post('bookmark', [BookmarkController::class, 'store']);
    Route::post('unbookmark', [BookmarkController::class, 'destroy']);
});

Route::get('products', [ProductController::class, 'index']);
