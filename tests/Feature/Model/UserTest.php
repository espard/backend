<?php

namespace Tests\Feature\Model;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_can_create_new_user()
    {
        $data = User::factory()->make()->toArray();
        $data['password'] = 123456789;

        User::create($data);

        $this->assertDatabaseCount('users', 1);
        $this->assertDatabaseHas('users', [
            'email' => $data['email'],
        ]);
    }

    public function test_user_with_product_relationship()
    {
        $count = rand(1, 10);

        $user = User::factory()
            ->hasProducts($count)
            ->create();

        $this->assertCount($count, $user->products);
        $this->assertTrue($user->products->first() instanceof Product);
    }
}
