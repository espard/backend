<?php

namespace Tests\Feature\Model;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_create_new_product()
    {
        $data = Product::factory()->make()->toArray();

        Product::create($data);

        $this->assertDatabaseCount('products', 1);
    }

    public function test_product_with_user_relationship()
    {
        $product = Product::factory()
            ->for(User::factory())
            ->create();

        $this->assertTrue(isset($product->user->id));
        $this->assertTrue($product->user instanceof User);
    }
}
