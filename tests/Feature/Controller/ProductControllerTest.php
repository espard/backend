<?php

namespace Tests\Feature\Controller;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_regular_user_cant_create_product()
    {
        $user = User::factory()->make()->toArray();
        $user['password'] = '123456789';
        $user = User::create($user);

        $response = $this
            ->actingAs($user, 'api')
            ->postJson(route('adminproduct.store'), Product::factory()->make()->toArray());

        $response->assertStatus(403);
    }

    public function test_create_product_validation()
    {
        $user = User::factory()->make()->toArray();
        $user['password'] = '123456789';
        $user['is_admin'] = '1';
        $user = User::create($user);

        $response = $this
            ->actingAs($user, 'api')
            ->postJson(route('adminproduct.store'));

        $response->assertStatus(422);
    }

    public function test_admin_can_create_product()
    {
        $user = User::factory()->make()->toArray();
        $user['password'] = '123456789';
        $user['is_admin'] = '1';
        $user = User::create($user);

        $response = $this
            ->actingAs($user, 'api')
            ->postJson(route('adminproduct.store'), Product::factory()->make()->toArray());

        $response->assertStatus(201);
    }
}
