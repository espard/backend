<?php

namespace App\Traits\V1;

trait ResponseGeneratorTrait {

    protected $hasError;
    protected $headerCode;

    public function setHasError($hasError)
    {
        $this->hasError = $hasError;

        return $this;
    }

    public function setHeaderCode($paramheadercode)
    {
        $this->headerCode = $paramheadercode;

        return $this;
    }

    public function setResponse($data)
    {
        return response()->json([
            'result' => [
                'error' => $this->hasError,
                'data' => $data
            ]
        ], $this->headerCode);
    }

    public function success($data, $headcode)
    {
        return $this->setHasError(false)
            ->setHeaderCode($headcode)
            ->setResponse($data);
    }

    public function error($data, $headcode)
    {
        return $this->setHasError(true)
        ->setHeaderCode($headcode)
        ->setResponse($data);
    }
}
