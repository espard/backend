<?php

namespace App\Jobs\V1\Bookmark;

use App\Http\Requests\V1\BookmarkRequest;
use App\Repositories\Bookmark\IBookmarkRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreBookmarkJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private IBookmarkRepository $bookmarkRepository;

    /**
     * Create a new job instance.
     */
    public function __construct(private BookmarkRequest $request)
    {
        $this->bookmarkRepository = app()->make(IBookmarkRepository::class);
    }

    /**
     * Execute the job.
     */
    public function handle(): mixed
    {
        $criteria = [
            'user_id' => $this->request->user()->id,
            'product_id' => $this->request->product_id
        ];

        $bookmarked = $this->bookmarkRepository->find($criteria);

        if ($bookmarked == null) {
            return $this->bookmarkRepository->create($criteria);
        }

        return false;
    }
}
