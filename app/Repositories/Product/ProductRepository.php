<?php

namespace App\Repositories\Product;

use App\Jobs\V1\Product\AddProductToCacheJob;
use App\Models\Product;
use App\Repositories\AbstractRepository;

class ProductRepository extends AbstractRepository implements IProductRepository
{
    protected $model = Product::class;

    public function getAll()
    {
        return parent::getAll()->load('user');
    }

    public function readOneById(int $id)
    {
        $item = $this->readOneFromCache($id);

        if($item == null){
            $item = $this->getOneById($id);
            AddProductToCacheJob::dispatch($item);
        }

        return $item;
    }

    private function readOneFromCache(int $id)
    {
        return null;
    }

    public function getOneById(int $id)
    {
        $item = parent::getOneById($id);
        if ($item !== null) {
            return $item->load('user');
        }
        return null;
    }
}
