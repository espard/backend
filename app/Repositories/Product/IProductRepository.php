<?php

namespace App\Repositories\Product;

interface IProductRepository
{
    public function getAll();
    public function getOneById(int $id);
    public function readOneById(int $id);
}
