<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository implements IAbstractRepository
{
    protected $model;

    protected function getModel()
    {
        return new $this->model;
    }

    public function create(array $attributes = []): Model
    {
        return $this->getModel()->create($attributes);
    }

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function find($criteria)
    {
        return $this->getModel()->where($criteria)->first();
    }

    public function getOneById(int $id) {
        return $this->getModel()->find($id);
    }

    public function update($item, array $data)
    {
        return $item->update($data);
    }

    public function delete($item)
    {
        return $item->delete();
    }
}
