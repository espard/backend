<?php

namespace App\Repositories\Bookmark;

interface IBookmarkRepository
{
    public function list($user_id);
    public function unbookmark($product_id, $user_id);
}
