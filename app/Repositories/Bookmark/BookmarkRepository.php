<?php

namespace App\Repositories\Bookmark;

use App\Models\ProductUser;
use App\Models\User;
use App\Repositories\AbstractRepository;

class BookmarkRepository extends AbstractRepository implements IBookmarkRepository
{
    protected $model = ProductUser::class;

    public function list($user_id)
    {
        return User::find($user_id)->bookmarks;
    }

    public function unbookmark($product_id, $user_id) {
        return $this->getModel()
            ->where('product_id', $product_id)
            ->where('user_id', $user_id)
            ->delete();
    }
}
