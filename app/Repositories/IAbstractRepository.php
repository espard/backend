<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

interface IAbstractRepository
{
    public function create(array $attributes = []): Model;
    public function getAll();
    public function find($cond);
    public function getOneById(int $id);
    public function update($item, array $data);
    public function delete($item);
}
