<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Product\CreateProductRequest;
use App\Http\Requests\V1\Product\UpdateProductRequest;
use App\Repositories\Product\IProductRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    private IProductRepository $productRepository;

    public function __construct()
    {
        $this->productRepository = app()->make(IProductRepository::class);
    }

    public function index(Request $request)
    {
        return $this->success($this->productRepository->getAll(), Response::HTTP_OK);
    }

    public function show(Request $request, int $product_id)
    {
        $product = $this->productRepository->readOneById($product_id);

        if ($product !== null) {
            return $this->success($product, Response::HTTP_OK);
        }

        abort(404);
    }

    public function store(CreateProductRequest $request)
    {
        $data = $request->only(['title', 'description']);
        $data['user_id'] = $request->user()->id;
        $newProduct = $this->productRepository->create($data);

        if ($newProduct !== null) {
            return $this->success($newProduct, Response::HTTP_CREATED);
        }

        return $this->error(null, Response::HTTP_BAD_REQUEST);
    }

    public function update(UpdateProductRequest $request, int $product_id)
    {
        $product = $this->productRepository->getOneById($product_id);

        if ($product !== null && $product->user_id == $request->user()->id) {
            $data = $request->only(['title', 'description']);
            $this->productRepository->update($product, $data);
            return $this->success($product, Response::HTTP_OK);
        }

        abort(404);
    }

    public function destroy(Request $request, int $product_id)
    {
        $product = $this->productRepository->getOneById($product_id);

        if ($product !== null && $product->user_id == $request->user()->id) {
            $this->productRepository->delete($product);
            return $this->success(null, Response::HTTP_NO_CONTENT);
        }

        abort(404);
    }
}
