<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Auth\UserRegisterRequest;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;

class UserRegisterController extends Controller
{
    public function store(UserRegisterRequest $request)
    {
        $validated = $request->validated();
        $validated['password'] = bcrypt($validated['password']);
        $user = User::create($validated);
        $token = $user->createToken('API Token')->accessToken;

        if ($user != null) {
            return $this->success(['user' => $user, 'token' => $token], Response::HTTP_CREATED);
        }

        return $this->error(null, Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
