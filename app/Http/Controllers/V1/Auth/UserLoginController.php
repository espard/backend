<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserLoginController extends Controller
{
    public function store(Request $request)
    {
        if (!auth()->attempt($request->all())) {
            return $this->error(null, Response::HTTP_UNAUTHORIZED);
        }

        $token = auth()->user()->createToken('authToken')->accessToken;

        return $this->success(['token' => $token], Response::HTTP_ACCEPTED);
    }
}
