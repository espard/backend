<?php

namespace App\Http\Controllers\V1;

use App\Events\V1\Bookmark\StoreBookmarkEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\V1\BookmarkRequest;
use App\Jobs\V1\Bookmark\DestroyBookmarkJob;
use App\Jobs\V1\Bookmark\StoreBookmarkJob;
use App\Repositories\Bookmark\IBookmarkRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BookmarkController extends Controller
{
    private IBookmarkRepository $bookmarkRepository;

    public function __construct()
    {
        $this->bookmarkRepository = app()->make(IBookmarkRepository::class);
    }

    public function index(Request $request)
    {
        $list = $this->bookmarkRepository->list($request->user()->id);

        if (count($list) > 0) {
            return $this->success($list, Response::HTTP_OK);
        }

        return $this->error(null, Response::HTTP_NO_CONTENT);
    }

    public function store(BookmarkRequest $request)
    {
        if ($bookmark = StoreBookmarkJob::dispatchSync($request)) {
            StoreBookmarkEvent::dispatch($bookmark);
            return $this->success($bookmark, Response::HTTP_CREATED);
        }

        abort(403);
    }

    public function destroy(BookmarkRequest $request)
    {
        if (DestroyBookmarkJob::dispatchSync($request)) {
            return $this->success(null, Response::HTTP_NO_CONTENT);
        }

        abort(403);
    }
}
