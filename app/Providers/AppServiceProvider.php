<?php

namespace App\Providers;

use App\Http\Requests\V1\BookmarkRequest;
use App\Repositories\Bookmark\BookmarkRepository;
use App\Repositories\Bookmark\IBookmarkRepository;
use App\Repositories\Product\IProductRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        app()->bind(IProductRepository::class, ProductRepository::class);
        app()->bind(IBookmarkRepository::class, BookmarkRepository::class);
    }
}
